﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(a4bigschool3.Startup))]
namespace a4bigschool3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
